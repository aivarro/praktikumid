package praktikum4;

public class Tabel4 {

	// 7x7 tabel
	public static void main(String[] args) {
		
		int tabeliSuurus = 5;

		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if (j == i){
					System.out.print("1 ");
				}else{
					System.out.print("0 ");
				}
				//System.out.print("i=" + i + "j=" + j + ")");
			}
			System.out.println();
		}
	}
}
//	public static void main(String[] args) {
//		final int x_size = 7;  // table size in x direction
//		final int y_size = 7;  // table size in y direction
//		
//		// Create the table
//		for (int x = 0; x <= x_size; x++) {
//			for (int y = 0; y <= y_size; y++) {
//				if (x == y) {
//					System.out.printf("1 ");
//				} else {
//					System.out.printf("0 ");
//				}
//				
//			}
//			//we are at the end of the line, start a new one
//			System.out.printf("%n");
//		}
//	}
//}
