package praktikum2;

import lib.TextIO;

public class Grupid {

	public static void main(String[] args) {
		
		int inimesed;
		int grupid;
		
		System.out.println("Palun sisesta inimeste arv");
		inimesed = TextIO.getlnInt();
		
		System.out.println("Palun sisesta gruppide arv");
		grupid = TextIO.getlnInt();
		
		int suurus = inimesed / grupid;
		System.out.println("Igas grupis on " + suurus + " inimest.");
		// int suurus = (int) Math.floor(inimesed / grupid);
		//System.out.println("Saab moodustada " + suurus + " gruppi.");
		
		int jääk = inimesed % grupid;
		System.out.println("Jääk on " + jääk);

	}

}
